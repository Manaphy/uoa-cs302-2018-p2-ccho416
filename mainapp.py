#!/usr/bin/python
""" mainapp.py

    COMPSYS302 - Software Design
    Author: Eric Chow (ccho416@aucklanduni.ac.nz)
    Last Edited: 19/02/2018

    This program uses the CherryPy web server (from www.cherrypy.org).
"""
# Requires:  CherryPy 3.2.2  (www.cherrypy.org)
#            Python  (We use 2.7)


# Importing libraries
import cherrypy, urllib2, json, socket, sqlite3, time, datetime, os, mimetypes, base64, math
from Crypto.Hash import SHA256
from jinja2 import Environment,FileSystemLoader
from urllib2 import urlopen
import threading

# global variables
global user
global hash
global thread

# Directory setup
CUR_DIR = os.path.dirname(os.path.abspath(__file__))
env = Environment(loader=FileSystemLoader(CUR_DIR))

# IP and port config
ip = socket.gethostbyname(socket.gethostname())
listen_ip = str(ip)
listen_port = 10003


class MainApp(object):
    #CherryPy Configuration
    _cp_config = {'tools.encode.on': True, 
                  'tools.encode.encoding': 'utf-8',
                  'tools.sessions.on' : 'True',
                 }
    # If they try somewhere we don't know, catch it here and send them to the right place.
    @cherrypy.expose
    def default(self, *args, **kwargs):
        """The default page, given when we don't recognise where the request is for."""
        Page = "I don't know where you're trying to go, so have a 404 Error."
        cherrypy.response.status = 404
        return Page

    @cherrypy.expose
    # Home page to display online users and the user's profile
    def index(self):
        try:
            username = cherrypy.session['username']
            self.updateOnlineUsers()

            # Getting the list of online and offline users
            db = sqlite3.connect('./data/main.db')
            cursor = db.cursor()
            online = []
            for row in cursor.execute('SELECT * FROM userlist'): #  table userlist contains online users
                online.append(row[0])   # add to list of online users
            db.close()
            db = sqlite3.connect('./data/main.db')
            cursor = db.cursor()
            userlist = []
            for row in cursor.execute('SELECT * FROM userprofile'): #  table userprofile contains all user profiles
                userlist.append(row[0]) # add to list of users
            db.close()
            userlist = sorted(userlist)
            offline = [i for i in userlist if i not in online]
            online_num = len(online)
            profile = json.loads(self.myProfile())
            template = env.get_template('index.html')
            return template.render(username = cherrypy.session['username'], profile = profile, online = online, offline = offline, online_num = online_num, userlist = userlist)
        except KeyError: #There is no username
            # Redirect to login page
            raise cherrypy.HTTPRedirect('/login')

    @cherrypy.expose
    # Page for displaying user profiles
    def profiles(self, **kwargs):
        template = env.get_template('profiles.html')
        try:
            username = cherrypy.session['username']
            # Retrieve profile from database if the argument username is a valid user
            if (self.validUser(kwargs['username']) == 0):
                db = sqlite3.connect('./data/main.db')
                cursor = db.cursor()
                userlist = []
                for row in cursor.execute('SELECT * FROM userprofile'): #  table userlist contains online users
                    userlist.append(row[0])
                db.close()
                userlist = sorted(userlist)
                profile = json.loads(self.getProfileUser(kwargs['username'], cherrypy.session['username'])) 
                return template.render(userlist = userlist, profile = profile)
            else:
                # Display a blank profile if there is no valid username argument
                kwargs['username'] = "None"
                profile = { 'fullname' : "",
                            'position' : "",
                            'description' : "",
                            'location' : "",
                            'picture' : "http://iabc.bc.ca/wp-content/uploads/2018/05/unknown_profile.png"
                    }
                db = sqlite3.connect('./data/main.db')
                cursor = db.cursor()
                userlist = []
                for row in cursor.execute('SELECT * FROM userprofile'):
                    userlist.append(row[0])
                db.close()
                userlist = sorted(userlist)
                return template.render(userlist = userlist, profile = profile)
        except KeyError: #There is no username
            raise cherrypy.HTTPRedirect('/profiles?username=None')
    @cherrypy.expose
    def messages(self, **kwargs):   # messages page
        template = env.get_template('messages.html')
        try:
            username = cherrypy.session['username']
            # Get a list of online and offline users
            db = sqlite3.connect('./data/main.db')
            cursor = db.cursor()
            online = []
            for row in cursor.execute('SELECT * FROM userlist'): 
                online.append(row[0])
            db.close()
            db = sqlite3.connect('./data/main.db')
            cursor = db.cursor()
            userlist = []
            for row in cursor.execute('SELECT * FROM userprofile'):
                userlist.append(row[0])
            db.close()
            userlist = sorted(userlist)
            offline = [i for i in userlist if i not in online]
            online_num = len(online)
            if (self.validUser(kwargs['username']) == 0):
                # Return the message history if there is a valid username argument
                print "this is a valid user"
                messagehistory = self.messageHistory(username, kwargs['username'])
                return template.render(online = online, offline = offline, online_num = online_num, kwargs = kwargs, messagehistory = messagehistory)
            else:
                messagehistory = ""
                return template.render(online = online, offline = offline, online_num = online_num, kwargs = kwargs, messagehistory = messagehistory)
        except KeyError: #There is no username
            raise cherrypy.HTTPRedirect('/messages?username=')

    @cherrypy.expose
    # Add a received file to the local database
    def addFile(self, sender=None, destination=None, file=None, filename=None, content_type=None, stamp=None):
        db = sqlite3.connect('./data/main.db')
        cursor = db.cursor()
        encoding = 0
        encryption = 0
        hashing = 0
        hash = ""
        decryptionKey = ""
        groupID = ""
        cursor.execute('''INSERT INTO messages(sender,destination,file,filename,content_type,stamp,encryption,hashing,hash,decryptionKey,groupID)
                          VALUES(?,?,?,?,?,?,?,?,?,?,?)''', (sender,destination,file,filename,content_type,stamp,encryption,hashing,hash,decryptionKey,groupID,))
        db.commit()
        db.close()
 
    @cherrypy.expose
    # Login page
    def login(self):
        template = env.get_template('login.html')
        return template.render()

    @cherrypy.expose
    # Error handler
    def error(self, code=None):
        options = {0 : "0",
            "1" : "Missing Compulsory Field",
            "2" : "Unauthenticated User",
            "3" : "Client Currently Unavailable",
            "4" : "Database Error",
            "5" : "Timeout Error",
            "6" : "Insufficient Permissions",
            "7" : "Hash does not match",
            "8" : "Encoding not supported",
            "9" : "Encryption Standard Not Supported",
            "10": "Hashing Standard Not Supported",
            "11": "Rated Limited",
            "12": "File size too large"
        }
        try:
            Page = code + ", " + options[code]
        except:
            Page = "Error Unknown!"
        return Page

    @cherrypy.expose
    @cherrypy.tools.json_in()
    # API function to receive a message from other clients
    def receiveMessage(self):
        input_data = cherrypy.request.json
        print "input data is: "
        print input_data
        if input_data["sender"] and input_data["destination"] and input_data["message"] and input_data["stamp"]:
            if (self.validUser(input_data["sender"]) == 0) and (self.validUser(input_data["destination"]) == 0):
                self.allowance(input_data["sender"])
                self.addText(input_data["sender"],input_data["destination"],input_data["message"])
                return "0"
            else:
                return "1, invalid sender/destination"
        else:
            return "1, invalid/missing field(s)"
 
    @cherrypy.expose
    @cherrypy.tools.json_in()
    # API function to receive a file from other clients
    def receiveFile(self):
        input_data = cherrypy.request.json
        print "input data is: "
        print input_data
        if input_data["sender"] and input_data["destination"] and input_data["file"] and input_data["filename"] and input_data["content_type"] and input_data["stamp"]:
            if (self.validUser(input_data["sender"]) == 0) and (self.validUser(input_data["destination"]) == 0):
                if (((len(input_data["file"]) * 3) / 4) > 5242880): # Check for 5mb file size limit
                    raise cherrypy.HTTPRedirect('/error?code=12')
                self.allowance(input_data["sender"])
                self.addFile(input_data["sender"],input_data["destination"],input_data["file"],input_data["filename"],input_data["content_type"],input_data["stamp"])
                return "0"
            else:
                return "1, not valid sender/destination"
        else:
            return "1, invalid/missing field(s)"
        
    @cherrypy.expose
    # Only called through the form submission on the messages.html page
    def sendMessage(self, destination=None, message=None, uploaded_file=None):
        sender = cherrypy.session['username']
        print "the sender is"
        print sender
        if ((self.validUser(destination) == 0) and (self.validUser(sender) == 0) and (len(message) > 0) and (message.find("<script>") == -1)) or uploaded_file.file:
            self.addMessage(sender, destination, message,uploaded_file)
            print "message sent!"
            raise cherrypy.HTTPRedirect('/messages?username='+destination)
        else:
            print "message unsuccessful :("
            raise cherrypy.HTTPRedirect('/messages?username='+destination)

    @cherrypy.expose
    # Adds text only to the databaase
    # This is for the receiveMessage API function
    def addText(self, sender, destination, message):
        db = sqlite3.connect('./data/main.db')
        cursor = db.cursor()
        stamp = int(time.time())
        encoding = 0
        encryption = 0
        hashing = 0
        hash = ""
        decryptionKey = ""
        groupID = ""
        file64 = None
        filename = None
        content_type = None
        cursor.execute('''INSERT INTO messages(sender,destination,message,file,filename,content_type,stamp,encoding,encryption,encoding,hashing,hash,decryptionKey,groupID)
                          VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', (sender,destination,message,file64,filename,content_type,stamp,encoding,encryption,encoding,hashing,hash,decryptionKey,groupID,))
        db.commit()
        db.close()

    @cherrypy.expose
    # Sends message and/or file to the recipient
    # Only called through the form submission on the messages.html page
    def addMessage(self, sender, destination, message, uploaded_file):
        # Adding message to local database
        
        if (uploaded_file.file):    # determines file attributes if there is a file
            file64 = str(base64.b64encode(uploaded_file.file.read()))
            if (((len(file64) * 3) / 4) > 5242880): # Check for 5mb file size limit
                raise cherrypy.HTTPRedirect('/error?code=12')
            content_type = mimetypes.guess_type(uploaded_file.filename, strict=True)[0]
            if (content_type is None):
                print "content is null"
                content_type = None
            filename = uploaded_file.filename
        else:
            file64 = None
            content_type = None
            filename = None
            print "no file here"

        # Store variables in local database    
        db = sqlite3.connect('./data/main.db')
        cursor = db.cursor()
        stamp = int(time.time())
        encoding = 0
        encryption = 0
        hashing = 0
        hash = ""
        decryptionKey = ""
        groupID = ""
        cursor.execute('''INSERT INTO messages(sender,destination,message,file,filename,content_type,stamp,encoding,encryption,encoding,hashing,hash,decryptionKey,groupID)
                          VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', (sender,destination,message,file64,filename,content_type,stamp,encoding,encryption,encoding,hashing,hash,decryptionKey,groupID,))
        db.commit()

        # Convert message variables to json
        dictionary = {}
        dictionary['sender'] = sender
        dictionary['destination'] = destination
        dictionary['message'] = message
        dictionary['stamp'] = stamp
        dictionary['encoding'] = encoding
        dictionary['hashing'] = hashing
        dictionary['hash'] = hash
        dictionary['decryptionKey'] = decryptionKey
        dictionary['groupID'] = groupID
        message_data = json.dumps(dictionary)

        # Sending message to the recipient
        for row in cursor.execute('SELECT * FROM userlist'):
            if destination == row[0] and (len(message) > 0):
                ip = str(row[1])
                port = str(row[4])
                url = "http://" + ip + ":" + port
                req = urllib2.Request(url + "/receiveMessage", message_data, {'Content-Type': 'application/json'})
                response = urllib2.urlopen(req).read()
                print "your url is: "
                print url
                print "response from message send: "
                print response
        
        # Convert file variables to json        
        file_dictionary = {}
        file_dictionary['sender'] = sender
        file_dictionary['destination'] = destination
        file_dictionary['file'] = file64
        file_dictionary['filename'] = filename
        file_dictionary['content_type'] = content_type
        file_dictionary['stamp'] = stamp
        file_dictionary['hashing'] = hashing
        file_dictionary['hash'] = hash
        file_dictionary['decryptionKey'] = decryptionKey
        file_dictionary['groupID'] = groupID
        file_data = json.dumps(file_dictionary)

        # Sending file to the recipient
        if (uploaded_file.file):
            for row in cursor.execute('SELECT * FROM userlist'):
                if destination == row[0]:
                    ip = str(row[1])
                    port = str(row[4])
                    url = "http://" + ip + ":" + port
                    try:
                        req = urllib2.Request(url + "/receiveFile", file_data, {'Content-Type': 'application/json'})
                        response = urllib2.urlopen(req).read()
                    except:
                        db.close()
                        raise cherrypy.HTTPRedirect('/error?code=3')
                    print "your url is: "
                    print url
                    print "response from file send: "
                    print response
        db.close()

    # LOGGING IN AND OUT
    @cherrypy.expose
    def signin(self, username=None, password=None):
        """Check their name and password and send them either to the main page, or back to the main login screen."""
        error = self.authoriseUserLogin(username,password)
        if (error == 0):
            global user
            global hash
            global thread
            thread = cherrypy.process.plugins.BackgroundTask(30, self.authoriseUserLogin, args=[username, password])
            thread.start()
            cherrypy.session['username'] = username;
            cherrypy.session['password'] = SHA256.new(password + username).hexdigest();
            user = cherrypy.session['username']
            hash = cherrypy.session['password']
            raise cherrypy.HTTPRedirect('/')
        else:
            raise cherrypy.HTTPRedirect('/error?code=2')

    @cherrypy.expose
    # Logs the current user out, expires their session, and cancels the thread
    def logoff(self):
        username = cherrypy.session.get('username')
        password = cherrypy.session.get('password')
        error = self.authoriseSignOut(username,password)
        if (error == 0):
            cherrypy.lib.sessions.expire()
            global thread
            thread.cancel()
            print "you have signed off!"
            raise cherrypy.HTTPRedirect('/')
        else:
            return "Couldn't sign off!"
       
    @cherrypy.expose
    # Show the edit profile page
    def editProfile(self):
        try:
            username = cherrypy.session['username']
            template = env.get_template('editprofile.html')
            return template.render()
        except:
            raise cherrypy.HTTPRedirect('/') 

    @cherrypy.expose
    @cherrypy.tools.json_in()
    def getProfile(self): # api for others sending json to get profile details
        input_data = cherrypy.request.json
        if (input_data["profile_username"] and input_data["sender"]):
            self.allowance(input_data["sender"])
            if (self.validUser(input_data["profile_username"]) == 0) and (self.validUser(input_data["sender"] == 0)):
                db = sqlite3.connect('./data/main.db')
                cursor = db.cursor()
                profile = {}
                cursor.execute("SELECT * FROM userprofile")
                for row in cursor.fetchall():
                    if row[0] == input_data["profile_username"]:
                        profile['fullname'] = row[1]
                        profile['position'] = row[2]
                        profile['description'] = row[3]
                        profile['location'] = row[4]
                        profile['picture'] = row[5]
                        if (row[6] != 0) and (row[6] != None):   
                            profile['encoding'] = row[6]
                        if (row[7] != 0) and (row[7] != None):
                            profile['encryption'] = row[7]
                        if (row[8] != "") and (row[8] != None):
                            profile['decryptionKey'] = row[8]
                        profile['lastUpdated'] = row[9]
                db.close()
                data = json.dumps(profile)
                Page = data
                return Page
            else:
                return "2: invalid profile_username or sender"
        else:
            return "1: Missing Compulsory Field"
        
    @cherrypy.expose
    # Retrieve data from local database using 2 inputs and doesn't
    # affect rate limit allowance
    def getProfileUser(self, profile_username=None, sender=None):
        db = sqlite3.connect('./data/main.db')
        cursor = db.cursor()
        profile = {}
        cursor.execute("SELECT * FROM userprofile")
        for row in cursor.fetchall():
            if row[0] == profile_username:
                profile['fullname'] = row[1]
                profile['position'] = row[2]
                profile['description'] = row[3]
                profile['location'] = row[4]
                profile['picture'] = row[5]
                if (row[6] != 0) and (row[6] != None):   
                    profile['encoding'] = row[6]
                if (row[7] != 0) and (row[7] != None):
                    profile['encryption'] = row[7]
                if (row[8] != "") and (row[8] != None):
                    profile['decryptionKey'] = row[8]
                profile['lastUpdated'] = row[9]
        db.close()
        data = json.dumps(profile)
        Page = data
        return Page
    
    @cherrypy.expose
    # Updating profile through form
    def setProfile(self, fullname=None, position=None, description=None, location=None, picture=None):
        print "your username is:  "
        try:
            username = cherrypy.session.get('username')
            if (len(picture) == 0):
                picture = "http://iabc.bc.ca/wp-content/uploads/2018/05/unknown_profile.png"      
            lastUpdated = int(time.time())
            db = sqlite3.connect('./data/main.db')

            cursor = db.cursor()
            encoding = 0
            encryption = 0
            decryptionKey = ""
            # updating profile
            cursor.execute("DELETE FROM userprofile WHERE username=?", (username,))
            cursor.execute('''INSERT INTO userprofile(username,fullname,position,description,location,picture,encoding,encryption,decryptionKey, lastUpdated)
                              VALUES(?,?,?,?,?,?,?,?,?,?)''', (username, fullname, position, description, location, picture, encoding, encryption, decryptionKey, lastUpdated,))
            db.commit()
            db.close()
            print "profile has been updated!"
            raise cherrypy.HTTPRedirect('/')
        except:
            raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    # Update profile in the local database (not through form)
    def updateProfile(self, username=None, fullname=None, position=None, description=None, location=None, picture=None, lastUpdated=None):
        print "updating profile for"
        print username
        
        if (picture == None):
            # Default picture if no picture url is provided
            picture = "http://iabc.bc.ca/wp-content/uploads/2018/05/unknown_profile.png"      
        db = sqlite3.connect('./data/main.db')
        cursor = db.cursor()
        encoding = 0
        encryption = 0
        decryptionKey = ""
        cursor.execute("DELETE FROM userprofile WHERE username=?", (username,))
        cursor.execute('''INSERT INTO userprofile(username,fullname,position,description,location,picture,encoding,encryption,decryptionKey, lastUpdated)
                          VALUES(?,?,?,?,?,?,?,?,?,?)''', (username, fullname, position, description, location, picture, encoding, encryption, decryptionKey, lastUpdated,))
        db.commit()
        db.close()
        print "profile has been updated!"
        raise cherrypy.HTTPRedirect('/profiles?username='+username)

    @cherrypy.expose
    # Retrieves profile information from the given user's database
    # Updates the local database with the most recent profile
    # This function is only used when clicking a view profile link (index.html and profile.html)
    def retrieveProfile(self, user=None):
        if (user == cherrypy.session.get('username')):
            raise cherrypy.HTTPRedirect('/profiles?username=' + user)
        db = sqlite3.connect('./data/main.db')
        cursor = db.cursor()
        toUpdate = [] 
        for row in cursor.execute('SELECT * FROM userlist'):
            if (row[0] == user) and (user != None):
                ip = str(row[1])
                port = str(row[4])
                url = "http://" + ip + ":" + port
                print url
                
                # check if other person is online
                try:
                    req = urllib2.Request(url + "/ping?sender=ccho416")
                    response = urllib2.urlopen(req).read()
                    print "their response is: "
                    print response[0]
                except:
                    db.close()
                    raise cherrypy.HTTPRedirect('/profiles?username=' + user)
                if (response[0] == "0"):
                    print "user" + str(row[0]) + " is online"
                    # Attempt to get their profile information from their database
                    try:
                        dictionary = {}
                        dictionary["profile_username"] = row[0]
                        dictionary["sender"] = cherrypy.session.get('username')
                        print dictionary
                        d_json = json.dumps(dictionary)
                        print d_json
                        
                        req = urllib2.Request(url + "/getProfile", d_json, {'Content-Type': 'application/json'})
                        their_response = urllib2.urlopen(req).read()
                        print "response is"
                        data = json.loads(their_response)
                        print data
                        if data.has_key('lastUpdated'):
                            print "their last updated is: "
                            print data['lastUpdated']
                            my_dictionary = {}
                            my_dictionary["profile_username"] = row[0]
                            my_dictionary["sender"] = "ccho416"
                            my_json = json.dumps(dictionary)
                            my_url = "http://" + listen_ip + ":" + str(listen_port)
                            my_req = urllib2.Request(my_url + "/getProfileUser?profile_username=" + my_dictionary["profile_username"] + "&sender=" + cherrypy.session['username'])
                            my_response = urllib2.urlopen(my_req).read()
                            my_data = json.loads(my_response)
                            print "my data: "
                            print my_data
                            if my_data.has_key('lastUpdated'):
                                my_lastUpdated = my_data['lastUpdated']
                            else:
                                db.close()
                                raise cherrypy.HTTPRedirect('/profiles?username=' + user)
                            print "my last updated is: "
                            print my_lastUpdated

                            # Update the local database with their information if their profile was updated more recently
                            if (data['lastUpdated'] > my_lastUpdated):
                                if data.has_key('picture'):
                                    picture = data["picture"]
                                else:
                                    picture = ""
                                    
                                if data.has_key('description'):   
                                    description = data["description"]
                                else:
                                    description = ""
                                    
                                if data.has_key('location'):
                                    location = data["location"]
                                else:
                                    location = ""
                                    
                                if data.has_key('position'):
                                    position = data["position"]
                                else:
                                    position = ""
                                    
                                if data.has_key('fullname'):
                                    fullname = data["fullname"]
                                else:
                                    fullname = ""
                                params = {"username" : row[0], "fullname" : fullname, "position" : position, "description" : description, "location" : location, "picture" : picture, "lastUpdated" : data['lastUpdated']}
                                toUpdate.append(params)
                    except:
                       db.close() 
                       raise cherrypy.HTTPRedirect('/profiles?username=' + user) 
                else:
                    db.close()
                    raise cherrypy.HTTPRedirect('/profiles?username=' + user)
        db.close()
        print "db close"
        print toUpdate
        print len(toUpdate)
        if len(toUpdate) != 0:
            self.updateProfile(toUpdate[0]['username'], toUpdate[0]['fullname'], toUpdate[0]['position'], toUpdate[0]['description'], toUpdate[0]['location'], toUpdate[0]['picture'], toUpdate[0]['lastUpdated'])
        else:
            raise cherrypy.HTTPRedirect('/profiles?username=' + user)
        
    @cherrypy.expose
    # Displays the current session's user profile (for the index.html page)
    def myProfile(self):
        db = sqlite3.connect('./data/main.db')
        cursor = db.cursor()
        profile = {}
        cursor.execute("SELECT * FROM userprofile")
        for row in cursor.fetchall():
            if row[0] == cherrypy.session.get('username'):
                profile['fullname'] = row[1]
                profile['position'] = row[2]
                profile['description'] = row[3]
                profile['location'] = row[4]
                profile['picture'] = row[5]
                profile['encoding'] = row[6]
                profile['encryption'] = row[7]
                profile['decryptionKey'] = row[8]
        db.close()
        data = json.dumps(profile)
        Page = data
        return Page

    def authoriseSignOut(self, username, password):
        # Attemps to sign the user out
        enc = "0";
        req = urllib2.Request("http://cs302.pythonanywhere.com/logoff?username=" + username + "&password=" + password + "&enc=" + enc)
        response = urllib2.urlopen(req).read()
        if (response[0] == "0"): 
            return 0
        else:
            return 1
        
    def authoriseUserLogin(self, username, password):
        # Attemps to log the user in the login server with their given credentials
        ip = socket.gethostbyname(socket.gethostname())
        if (ip[0:6] == "10.103"):
            location = "0"
            listen_ip = socket.gethostbyname(socket.gethostname())
        elif (ip[0:7] == "118.92."):
            location = "2"
            listen_ip = urlopen('http://ip.42.pl/raw').read()
        elif (ip[0:7] == "192.168"):
            location = "2"
            listen_ip = urlopen('http://ip.42.pl/raw').read()
        elif (ip[0:7] == "121.74."):
            location = "2"
            listen_ip = urlopen('http://ip.42.pl/raw').read()
        else:
            location = "1"
            listen_ip = socket.gethostbyname(socket.gethostname())
        username = username.lower()
        password_hash = SHA256.new(password + username).hexdigest()
        enc = "0"
        try:
            req = urllib2.Request("http://cs302.pythonanywhere.com/report?username=" + username + "&password=" + password_hash + "&location=" + location + "&ip=" + listen_ip + "&port=" + str(listen_port) + "&enc=" + enc)
            response = urllib2.urlopen(req).read()
        except:
            return "invalid credentials"
        print listen_ip
        print "response is: "
        print response
        print location
        if (response[0] == "0"): 
            return 0
        else:
            return 1
    
    @cherrypy.expose
    # Gets online users from the login server
    def updateOnlineUsers(self):
        try:
            db = sqlite3.connect('./data/main.db')
            cursor = db.cursor()
            j = json.loads(urllib2.urlopen("http://cs302.pythonanywhere.com/getList?username=" + cherrypy.session['username'] + "&password=" + cherrypy.session['password'] + "&enc=0&json=1").read())
            cursor.execute('SELECT * FROM userlist')
            cursor.execute('DELETE FROM userlist')
            db.commit()
            for i in range(0,len(j)):
                username = j[str(i)]["username"]
                ip = j[str(i)]["ip"]
                location = j[str(i)]["location"]
                lastLogin = j[str(i)]["lastLogin"]
                port = j[str(i)]["port"]
                cursor.execute('''INSERT INTO userlist(username,ip,location,lastLogin,port)
                                  VALUES(?,?,?,?,?)''', (username, ip, location, lastLogin, port,))
                db.commit()
            db.close()
            return "0, online users have been updated!"
            print "datbase updated!"
        except:
            return "2, unauthenticated user"

    def messageHistory(self, username, other_person):
        # Returns the message history between 2 people
        print "otherperson is: "
        print other_person
        db = sqlite3.connect('./data/main.db')
        cursor = db.cursor()
        table = cursor.execute("SELECT * FROM messages")
        Page = ""
        for row in table:
            # Display text message
            if (row[1] == other_person) and (row[2] == username): # sender       destination
                Page += "From: " + row[1] + "<br>"
                Page += str(datetime.datetime.fromtimestamp(row[7]).strftime('%c')) + "<br>"
                if (row[3] != None):
                    Page += row[3] + "<br>"
                # Display file
                if (row[4] != None) and (len(row[4]) > 100):
                    if(row[6][0:5] == "image"):
                        Page += '<img src="data:image/png;base64,' + row[4] + '" alt="image" width="200" height="200"/>'
                        Page += "<br>"
                    elif(row[6][0:15] == "application/pdf"):
                        Page += '<embed src="data:application/pdf;base64,' + row[4] + '" alt="pdf"/>'
                        Page += "<br>"
                    elif(row[6][0:5] == "audio"):
                        Page += '<audio controls="controls" autobuffer="autobuffer"><source src="data:' + row[6] + ';base64,' + row[4] + '" /></audio>'
                        Page += "<br>"
                    elif(row[6][0:5] == "video"):
                        Page += '<video src="data:' + row[6] + ';base64,' + row[4] + '" controls width=400 height=200></video>'
                        Page += "<br>"
                Page += "<br>"
            elif (row[1] == username) and (row[2] == other_person):
                Page += "To: " + row[2] + "<br>"
                Page += str(datetime.datetime.fromtimestamp(row[7]).strftime('%c')) + "<br>"
                # Display text message
                if (row[3] != None):
                    Page += row[3] + "<br>"
                # Display file
                if (row[4] != None) and (len(row[4]) > 100):
                    if(row[6][0:5] == "image"):
                        Page += '<img src="data:image/png;base64,' + row[4] + '" alt="image" width="200" height="200"/>'
                        Page += "<br>"
                    elif(row[6][0:15] == "application/pdf"):
                        Page += '<embed src="data:application/pdf;base64,' + row[4] + '" alt="pdf"/>'
                        Page += "<br>"
                    elif(row[6][0:5] == "audio"):
                        Page += '<audio controls="controls" autobuffer="autobuffer"><source src="data:' + row[6] + ';base64,' + row[4] + '" /></audio>'
                        Page += "<br>"
                    elif(row[6][0:5] == "video"):
                        Page += '<video src="data:' + row[6] + ';base64,' + row[4] + '" controls width=400 height=200></video>'
                        Page += "<br>"
                Page += "<br>"
        db.close()    
        return Page
    
    def allowance(self, sender):
        # Rate limit of 15 requests per 5 minutes
        rate = 15 # requests
        per = 300  # seconds
        db = sqlite3.connect('./data/main.db')
        cursor = db.cursor()
        for row in cursor.execute('SELECT * FROM ratelimit'):
            if row[0] == sender:
                current_time = int(time.time())
                time_passed = current_time - row[2]
                allowance = row[1]
                print "allowance before"
                print allowance 
                allowance += time_passed * (float(rate) / per)
                print "allowance after"
                print allowance
                print str(allowance)
                if (allowance > rate):
                    # Maximum allowance (10 requests) is set if too much time passes
                    allowance = rate
                if (math.floor(allowance) <= 0):
                    # Rate limited
                    allowance = 0
                    db.close()
                    raise cherrypy.HTTPRedirect('/error?code=11')
                else:
                    allowance -= 1
                if (allowance != 0):
                    # Update database
                    cursor.execute("DELETE FROM ratelimit WHERE username=?", (sender,))
                    cursor.execute('''INSERT INTO ratelimit(username,allowance,lastUpdated)
                                      VALUES(?,?,?)''', (sender,int(allowance), current_time,))
        db.commit()                    
        db.close()
    
    @cherrypy.expose
    # Always returns 0 and allows other clients to check if this server is running
    def ping(self, sender=None):
        if (sender==None):
           raise cherrypy.HTTPRedirect('/error?code=1') 
        Page = "0"
        return Page 

    def validUser(self, user):
        db = sqlite3.connect('./data/main.db')

        cursor = db.cursor()

        users = []

        for row in cursor.execute('SELECT * FROM userprofile'):
            users.append(row[0])
        db.close()
        if user in users:
            return 0
        else:
            return 2

        
def runMainApp():
    # Create an instance of MainApp and tell Cherrypy to send all requests under / to it. (ie all of them)
    conf = { '/': {
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
             '/assets': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './assets'
        }
        }

    cherrypy.tree.mount(MainApp(), "/", conf)
    
    # Tell Cherrypy to listen for connections on the configured address and port.
    cherrypy.config.update({'server.socket_host': listen_ip,
                            'server.socket_port': listen_port,
                            'engine.autoreload.on': True,
                           })
    #cherrypy.quickstart(MainApp())
    print "========================="
    print "University of Auckland"
    print "COMPSYS302 - Software Design Application"
    print "========================================"                       
    # Start the web server
    cherrypy.engine.start()

    # And stop doing anything else. Let the web server take over.
    cherrypy.engine.block()

    # Log off if the system shuts down
    if cherrypy.engine.state != cherrypy.engine.states.STARTED:
        try:
            enc = "0";
            req = urllib2.Request("http://cs302.pythonanywhere.com/logoff?username=" + user + "&password=" + hash + "&enc=" + enc)
            response = urllib2.urlopen(req).read()
            if (response[0] == "0"): 
                print "logoff successful!"
            else:
                return "unsuccessful logout"
        except:
            print "no user to log out"
            
        
#Run the function to start everything
runMainApp()
