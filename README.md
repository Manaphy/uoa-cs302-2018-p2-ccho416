## CS302 Messaging Server

This server allows clients to send files, messages and view information on other clients.

---

## Requirements

Only [Python 2.7](https://www.python.org/download/releases/2.7/) is required. Any other external libraries (CherryPy 3.7, PyCrypto 2.6.1, Jinja2) are included in this repository.

---

## Installation

Download the repository as a zip file via the 'Downloads' tab on the left side and extract to desktop. Alternatively you may clone the repository.
Use these steps to clone from SourceTree. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

---

## Set up

Now that you have the files installed, install the external packages (CherryPy 3.7, PyCrypto 2.6.1, Jinja2) by visiting each of the respective folders and executing on command line:
```
python setup.py install
```

Once you have install the external libraries, go back to the root directory and run the mainapp.py file with Python (command line).

```
python mainapp.py
```

Visit the URL given in the terminal (eg http://192.168.1.14:10003/) on a browser.
You should see a login screen. Enter your login details and submit, then you should be good to go!

## Supported Clients

The system has been tested for the following clients: 
pqua613, acha932